 How would you like to connect to Ionic Pro? Automatically setup new a SSH key pair for Ionic Pro
[INFO] The automatic SSH setup will do the following:
       1) Generate a new SSH key pair with OpenSSH (will not overwrite any existing keys).
       2) Upload the generated SSH public key to our server, registering it on your account.
       3) Modify your SSH config (..\Users\SWN_Dev\.ssh\config) to use the generated SSH private key for our server(s).

? May we proceed? Yes
> ionic ssh generate C:\Users\SWN_Dev\.ssh\ionic\22391_rsa
[ERROR] Command not found: ssh
[WARN] OpenSSH not found on your computer.
[INFO] Created ..\Users\SWN_Dev\.ssh\ionic directory for you.

[INFO] You will be prompted to provide a passphrase, which is used to protect your private key should you lose it. (If
       someone has your private key, they can impersonate you!) Passphrases are recommended, but not required.
'ssh-keygen' is not recognized as an internal or external command,
operable program or batch file.
[ERROR] Command not found: ssh-keygen

The authenticity of host 'git.ionicjs.com (35.160.140.25)' can't be established.
ECDSA key fingerprint is SHA256:Wv6rYBk1T3XSPfybwccdQJ+lhmR+FC6F61d1i46VPFI.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'git.ionicjs.com,35.160.140.25' (ECDSA) to the list of known hosts.
Permission denied (publickey).
fatal: Could not read from remote repository.